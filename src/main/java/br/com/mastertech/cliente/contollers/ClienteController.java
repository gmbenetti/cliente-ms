package br.com.mastertech.cliente.contollers;


import br.com.mastertech.cliente.models.Cliente;
import br.com.mastertech.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @GetMapping
    public List<Cliente> buscarTodos(){
        List<Cliente> clientes = clienteService.buscarTodos();

        return clientes;
    }

    @GetMapping("/{idCliente}")
    public Cliente buscarPorId(@PathVariable(name="idCliente")Long idCliente){
        try{
            Cliente cliente = clienteService.buscarPorId(idCliente);
            return cliente;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PostMapping
    public Cliente registraCliente(@RequestBody @Valid Cliente cliente){
        Cliente clienteObjeto = clienteService.salvar(cliente);
        return clienteObjeto;
    }

}
